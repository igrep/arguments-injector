{-# LANGUAGE DeriveGeneric #-}

module Main where


import           Control.Applicative ((<|>))
import           Control.Exception (throwIO)
import           Control.Monad (unless)
import qualified Data.Char as C
import qualified Data.List as L
import qualified Data.Map as M
import qualified Data.Yaml as Y
import           GHC.Generics (Generic)
import qualified System.Directory as D
import qualified System.Environment as E
import qualified System.Process as P


data Config =
  Config
    { originalPath :: Maybe FilePath
    , commands :: M.Map String Profile
    } deriving Generic

instance Y.FromJSON Config
instance Y.ToJSON Config


data Profile =
  Profile
    { append :: [String]
    } deriving (Generic, Show)

instance Y.FromJSON Profile
instance Y.ToJSON Profile


trueBinName :: String
trueBinName = "arguments-injector"


getConfigDirectory :: IO FilePath
getConfigDirectory = D.getAppUserDataDirectory trueBinName


getOriginalBinDirectory :: FilePath -> Config -> FilePath
getOriginalBinDirectory defaultDir =
  maybe defaultDir id . originalPath


defaultOriginalBinDirectory :: FilePath -> FilePath
defaultOriginalBinDirectory configDir =
  configDir ++ "/" ++ "original-bin/"


exampleConfig :: FilePath -> Config
exampleConfig defaultOrigBinDir =
  Config (Just defaultOrigBinDir) $
    M.fromList [("some-profile-enabled-command", Profile ["+RTS", "-xc", "-RTS"])]


lookupBinName :: String -> Config -> Maybe Profile
lookupBinName binName cfg =
  let cmds = commands cfg
  in
    M.lookup binName cmds <|> M.lookup (map C.toLower binName) cmds
    --                                  ^^^^^^^^^^^^^^^^^^^^^ for Windows


main :: IO ()
main = do
  origBinName <- E.getProgName
  cfgDir <- getConfigDirectory
  let defaultOrigBinDir = defaultOriginalBinDirectory cfgDir
      cfgPath = cfgDir ++ "/" ++ "config.yaml"

  let (origBinNameWithoutExtension, _) = L.break (== '.') origBinName
  if trueBinName == origBinNameWithoutExtension
    then do
      cfgFileCreated <- D.doesFileExist cfgPath
      unless cfgFileCreated $ do
        putStrLn "No config file found. Creating..."
        D.createDirectoryIfMissing True defaultOrigBinDir
        Y.encodeFile cfgPath $ exampleConfig defaultOrigBinDir
      putStrLn "Usage:"
      putStrLn $ "1. Modify " ++ cfgPath
      putStrLn "2. Rename this command into a command you want to modify the arguments"
      putStrLn "3. Then put it into a directory in PATH."
    else do
      eCfg <- Y.decodeFileEither cfgPath
      case eCfg of
          Right cfg -> do
            let origBinDir = getOriginalBinDirectory defaultOrigBinDir cfg
            case lookupBinName origBinName cfg of
                Just profile -> do
                  origArgs <- E.getArgs
                  P.callProcess (origBinDir ++ "/" ++ origBinName) $ origArgs ++ append profile
                _ ->
                  error $ trueBinName ++ ": No command '" ++ origBinName ++ "' not defined! See `commands' in " ++ cfgPath ++ " for available comands!"
          Left e -> do
            throwIO e
