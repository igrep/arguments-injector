# Arguments Injector

## Use Case

TODO

## Usage

Target command: The command you want to append/prepend arguments to.

1. Create a configuration file in `~/.arguments-injector/config.yaml`.
    - See below for details.
1. Move the target command to `~/.arguments-injector/original-bin/` 
    - Or the path specified in `~/.arguments-injector/config.yaml`. See below. 
    - E.g. `sudo mv /usr/bin/haddock ~/.arguments-injector/original-bin/`
1. Rename arguments-injector into the name of target command. Then put to `PATH`
    - E.g. `mv arguments-injector ~/bin/haddock`
1. Run the renamed arguments-injector.

## Sample Configuration

Put a YAML file like below to `~/.arguments-injector/config.yaml` (`%APPDATA%/arguments-injector/config.yaml` on Windows).

```yaml
# Directory where to put the original commands.
# Default:
#   - ~/.arguments-injector/original-bin/ on Unix
#   - %APPDATA%/arguments-injector/original-bin/ on Windows
originalPath: /path/to/original/bin

# Specify command name and extra options to pass
commands:
    haddock:
      append: ["+RTS", "-xc", "-RTS"] # By Array of Strings.
      # prepend: [] not implemented so far.
```
